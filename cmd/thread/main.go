package main

import (
	"fmt"
	//"time"
	"runtime"
)

func sum(a []int, c chan int) {
	total := 0
	for _, v := range a {
		total += v
	}
	c <- total
}

func say(s string) {
	for i := 0; i < 5; i++ {
		runtime.Gosched()
		fmt.Println(s)
	}
}

func fibonacci(n int, c chan int) {
	x, y := 1, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x+y
	}
	close(c)
}

//##########################
func fibonacci2(c, quit chan int) {
	x, y := 1, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			break
		}
	}
}

func main() {
	go say("world") //新しいGoroutinesを実行する。
	say("hello")    //現在のGoroutines実行
	//time.Sleep(3 * time.Second)
	a := []int{1, 2, 3, 4, 5, 6, 7, 8}

	c := make(chan int)
	go sum(a[:len(a)/2], c)
	go sum(a[len(a)/2:], c)
	x, y := <-c, <-c

	fmt.Println(x, y, x+y)

	bc := make(chan int, 3)
	bc <- 3
	bc <- 4
	bc <- 5
	fmt.Println(<-bc)
	fmt.Println(<-bc)
	fmt.Println(<-bc)

	fbc := make(chan int, 10)
	go fibonacci(cap(fbc), fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	// fmt.Println(<-fbc)
	for i := range fbc {
		fmt.Println(i)
	}

	cc := make(chan int)
	quit := make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-cc)
		}

		quit <- 0
	}()

	fibonacci2(cc, quit)

}
